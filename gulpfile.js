var gulp = require('gulp'),
    gutil = require('gulp-util'),
    uglifyCSS = require('gulp-uglifycss'),
    uglifyJS = require('gulp-uglify'),
    maps = require('gulp-sourcemaps'),
    prefixer = require('autoprefixer-core'),
    postcss = require('gulp-postcss'),
    concat = require('gulp-concat'),
    less = require('gulp-less'),
    imagemin = require('gulp-imagemin'),
    pngquant = require('imagemin-pngquant');

var paths = {
      css :
          [
            "bower_components/bootstrap/dist/css/bootstrap.min.css",
            "bower_components/font-awesome/css/font-awesome.min.css",
            "bower_components/animate.css/animate.min.css",
            "bower_components/simplebar/src/simplebar.css"
          ],
      js :
        {
          plugins: [
            "plugins/**/*.js",
            "bower_components/simplebar/src/simplebar.js",
            "bower_components/mixitup/src/jquery.mixitup.js"
          ],
          vendor: [
            "bower_components/jquery/dist/jquery.min.js",
            "bower_components/bootstrap/dist/js/bootstrap.min.js"
          ],
          app : ["core/**/*.js"]
        },
      fonts : "bower_components/font-awesome/fonts/**/*",
      less : ["less/style.less", "less/**/*.less"],
      style : "css/style.css"
};

// init task
gulp.task('init', ['js', 'vendor', 'less', 'copyfonts', 'image']);

// copy fontawesome fonts task
gulp.task('copyfonts', function() {
	return gulp.src(paths.fonts).
		pipe(gulp.dest('views/css/fonts'));
});

// less task
gulp.task('less', function(){
	var l = less();

	l.on('error', function(e){
		gutil.log(e);
		this.emit('end');
	});
	return gulp.src(paths.less[0]).
		pipe(l).
    pipe(postcss([prefixer({ browsers: ["> 0%"] })])).
		pipe(uglifyCSS()).
		pipe(gulp.dest('views/css'))
});

// vendor task
gulp.task('vendor', function(){
	return gulp.src(paths.css).
		pipe(concat('vendor.css')).
		pipe(uglifyCSS()).
		pipe(gulp.dest('views/css/vendor'));
});

// javascript task
gulp.task('js', ['js_vendor', 'js_plugins'], function(){
	return gulp.src(paths.js.app).
		pipe(concat('app.min.js')).
		pipe(uglifyJS()).
		pipe(gulp.dest('views/js'));
});

gulp.task('js_vendor', function(){
  return gulp.src(paths.js.vendor).
    pipe(concat('vendor.min.js')).
    pipe(uglifyJS()).
    pipe(gulp.dest('views/js'));
});

gulp.task('js_plugins', function(){
  return gulp.src(paths.js.plugins).
    pipe(concat('plugins.min.js')).
    pipe(uglifyJS()).
    pipe(gulp.dest('views/js'));
});

gulp.task('image', function () {
    return gulp.src('images/**/*')
        	.pipe(imagemin({
        	    progressive: true,
        	    svgoPlugins: [{ removeViewBox: false }],
        	    use: [pngquant()]
        	}))
        .pipe(gulp.dest('views/img'));
});

// watcher
gulp.task('watch', function(){
	gulp.watch(paths.js.app, ['js']);
  gulp.watch(paths.js.plugins, ['js']);
	gulp.watch(paths.css, ['vendor']);
	gulp.watch(paths.fonts, ['copyfonts']);
	gulp.watch(paths.less[1], ['less']);
  gulp.watch('images/**/*', ['image']);
});

gulp.task('default', ['init', 'watch']);