function setFitScreen() {
	'use strict';
	var galleryHeight = $('.aspiration-gallery > div').innerWidth(),
		galleryWrapper = $('.aspiration-gallery .wrapper');

	$('.table-row>[class*="col-"]').css({
		height: $(window).innerHeight()
	});

	galleryWrapper.css({
		height: galleryHeight- 30,
		width: galleryHeight - 30
	});

	$('.aspiration-gallery > div').css({
		height: galleryHeight
	})

	$('nav .navigation-content').css({
		height: $(window).innerHeight() - $('nav .navbar-header').height()
	});
}