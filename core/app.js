($(function(){
	$(document).on('ready', function(){

		'use strict';

		var fileType = "",
			allowExt = ['jpg', 'jpeg', 'png'];

		// video handler
		$(document).on('click', 'a.play-vd', function(e){
			e.preventDefault();

			$('a.btn-vd-play > .fa').removeClass('fa-spinner fa-spin');
			$('a.btn-vd-play > .fa').addClass('fa-play-circle');
			$('body').toggleClass('loading');
			$(".vd-thumbnail").show();
			$('#vd-player').fadeIn('slow');
		});

		$(document).on('click', '.vd-thumbnail > .btn-vd-play', function(e){
			e.preventDefault();

			var $this = $(this),
				video_id = $this.parent().find('img').attr('src').split('/')[4],
				iframe_vd = $('<div class="vd-container"><a href="#" class="vd-close"><img src="img/close.png"></a><iframe id="yt-ply" src="https://www.youtube.com/embed/' + video_id + '?rel=0&amp;controls=1&amp;showinfo=0&amp;modestbranding=1&amp;autoplay=1" allowfullscreen></iframe></div>');

			$this.find('.fa').removeClass('fa-play-circle');
			$this.find('.fa').addClass('fa-spinner fa-spin');	

			$('div.vd-container').remove();
			$('#vd-player').append(iframe_vd);
			$("#yt-ply").on('load', function(){
				$this.parent().fadeOut('slow');

				$('a.vd-close').on('click', function(e){
					e.preventDefault();

					$('#vd-player').fadeOut('slow', function(){
						$('body').toggleClass('loading');
						$('div.vd-container').remove();
					});
				});
			});
		});

		$(document).on('click', 'a.vd-close', function(e){
			e.preventDefault();

			$('#vd-player').fadeOut('slow', function(){
				$('body').toggleClass('loading');
				$('div.vd-container').remove();
			});
		});

		// navigation handler
		$(document).on('click', 'button.navbar-toggle', function(){
			var el = $(this),
				parent = el.parents('nav.navbar'),
				data = el.data('target');

			$('body').toggleClass('loading');

			if(parent.hasClass('opened'))
			{
				$(data).stop().animate({
					opacity: 0
				}, 200, function(){
					$(data).hide();
				});
				parent.removeClass('opened');
			}
			else {
				$(data).stop().show().animate({
					opacity: 1
				}, 200);
				parent.addClass('opened');
			}
		});

		// slider handler
		$(document).on('change', 'input[type=range', function(){
			var parent = $(this).parent(),
				val = $(this).val();

			if(val == 0)
				parent.removeClass().addClass('slider-opt left');
			else if(val == 1)
				parent.removeClass().addClass('slider-opt center');
			else
				parent.removeClass().addClass('slider-opt right');
		});

		// form submit handler
		
		// $("form#form-submit").on('submit', function(e){
		// 	e.preventDefault();

		// 	var formData = new FormData($(this)[0]);

		// 	$.ajax({
		//         url: $(this).attr('action'),
		//         type: 'POST',
		//         data: formData,
		//         async: false,
		//         success: function (data) {
		//             alert(data)
		//         },
		//         error: function(e) {
		//         	console.log(e);
		//         },
		//         cache: false,
		//         contentType: false,
		//         processData: false
		//     });

		//     return false;
		// });

		function readImg(input) {
			if (input.files && input.files[0]) {
			    var reader = new FileReader();
			    reader.onload = function(e) {
			      $('img.img-preview').attr('src', e.target.result);
			    }

			    reader.readAsDataURL(input.files[0]);
			}
		}

		$(document).on('change', "form#form-submit input[type=file]", function(e){
			var labelName = $('.browse-image p.form-control-static');

			fileType = $(this).val().split('.').pop();

			if(allowExt.indexOf(fileType.toLowerCase()) >= 0)
			{
				readImg(this);
				labelName.text($(this).context.files[0].name);
			}
			else {
				labelName.text('Unggah foto');
				alert('Wrong file ulpoad!');
			}
		});

		$(document).on('keyup', "form#form-submit textarea.form-control", function(e){
			$('.preview-form .desc-preview p').text($(this).val());
		});

		$(document).on('click', "a.edit-form, a.form-continue", function(e){
			e.preventDefault();
			if(allowExt.indexOf(fileType.toLowerCase()) >= 0 && $('form#form-submit textarea.form-control').val().length> 0)
				$('.preview-form, .form-panel').toggleClass('show');
			else
				alert('Field masih kosong!');
		});

		// gallery handler
		$(".aspiration-gallery").mixItUp({
            selectors: {
                target: ".gallery-view"
            },
            animation: {
                effects: "fade stagger scale rotateX(-360deg)",
                easing: "cubic-bezier(0.215, 0.61, 0.355, 1)"
            }
        });

        setFitScreen();

		$(window).on('resize', setFitScreen);

	});
}));